# RPIbackup Scripts - README #
---

### Overview ###

The **RPIbackup** scripts (Bash) will copy all the data from the standard/main SDcard of the Raspberry Pi to a backup SDcard installed at an USB port.

### Screenshots ###

![RPIbackup Screen 1](development/readme/rpibackup01.png "RPIbackup Screen 1")

![RPIbackup Screen 2](development/readme/rpibackup02.png "RPIbackup Screen 2")

### Versions ###

There are two versions of the **RPIbackup** scripts:

#### Guided Backup Script (backup_pi.sh)####
The first version is a guided backup script which has to be started manually and has different menus to choose from and you can do the following:

* Check the connected USB block devices
* Choose the backup device
* Initialize the backup device (create label, partitions, format partitions)
* Backup the main SDcard to the backup SDcard.
* Check the logfile

#### Automated Version (autobackup_pi.sh) ####
The second version is the automated version which can be run from a cronjob but has less options. The script does the following:

* Backup the main SDcard to the specified backup SDcard.

### Setup - Guided Backup Script ###

* Install a second SDcard to one of the USB ports of your Raspberry Pi.
* Copy the backup scripts **backup_pi.sh** to your Raspberry Pi.
* Make the script executable: **chmod +x backup_pi.sh**.
* Start the guided backup script with root permissions: **./backup_pi.sh**
* Follow the indications on the screen.

### Setup - Automated Backup Script ###

* Install a second SDcard to one of the USB ports of your Raspberry Pi.
* Copy the backup scripts **autobackup_pi.sh** to your Raspberry Pi.
* Make the script executable: **chmod +x autobackup_pi.sh**.
* Get the name of your backup device (in the following example 'sda').
* Start the automatic backup script with root permissions: **./autobackup_pi.sh sda**
* Check the output of the backup script for errors.
* Create a cronjob for the backup task.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **RPIbackup** scripts are licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
