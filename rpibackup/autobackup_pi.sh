#!/bin/bash


########################################################################
#                                                                      #
#                                                                      #
#          R A S P B E R R Y   P I   -   A U T O B A C K U P           #
#                                                                      #
#                   S D C A R D   T O   S D C A R D                    #
#                    S Y N C H R O N I Z A T I O N                     #
#                                                                      #
#                   A U T O M A T I C   B A C K U P                    #
#                                                                      #
#                      Copyright 2014 by PB-Soft                       #
#                                                                      #
#                           www.pb-soft.com                            #
#                                                                      #
#                                                                      #
########################################################################
#                                                                      #
#      ! ! YOU HAVE TO RUN THIS SCRIPT WITH ROOT PERMISSIONS ! !       #
#                                                                      #
########################################################################
#                                                                      #
#  This bash script will do the following:                             #
#                                                                      #
#  - Create a debug logfile (if the option is enabled).                #
#                                                                      #
#  - Check the size of the first (boot) partition of the backup de-    #
#    vice and if the 'backup_pi' script directory does exist on the    #
#    second (root) partition. It also checks that the number of par-   #
#    titions on the backup device is correct. That is necessary to     #
#    be sure that the inserted and selected device is really the       #
#    desired backup device because the device name can change after    #
#    a reboot of the Raspberry Pi computer.                            #
#                                                                      #
#  - Mount and unmount the necessary partitions (root and boot).       #
#                                                                      #
#  - Automatic synchronize the data of the main SDcard of the          #
#    Raspberry Pi to the backup SDcard in a USB card reader. There     #
#    is also a 'dry-run' option from the 'rsync' tool available to     #
#    test the backup process without writing any data.                 #
#                                                                      #
#  - Stop the specified services before the backup and start them      #
#    again after the backup has completed.                             #
#                                                                      #
#  - Send an e-mail message with the content of the logfiles in the    #
#    e-mail message body.                                              #
#                                                                      #
########################################################################
#                                                                      #
#  To run the script you have to specify the name of the backup de-    #
#  vice as a parameter. If your backup device is '/dev/sda', you have  #
#  to start the script like in the following example:                  #
#                                                                      #
#                      Example: autobackup.sh sda                      #
#                                                                      #
########################################################################


# ======================================================================
# C O N F I G U R A T I O N   -   P L E A S E   C U S T O M I Z E
# ======================================================================

# ======================================================================
# Specify if the test mode is enabled (0 = disabled / 1 = enabled). If
# the test mode is enabled, no data will be written to the SDcard. The
# synchronization tool 'rsync' will be started with the command line
# parameter '--dry-run' which will write to the logfile 'rsync.log',
# which files would be copied to the backup device.
# ======================================================================
TEST_MODE=1


# ======================================================================
# Specify if the debug mode is enabled (0 = disabled / 1 = enabled). If
# the debug mode is enabled, important information will be written to
# the autobackup logfile 'autobackup.log'. If the debug mode is dis-
# abled, no information is logged to this logfile. The synchronization
# logfile 'rsync.log' will be created anyway.
# ======================================================================
DEBUG_MODE=1


# ======================================================================
# Specify if the synchronizing is enabled (0 = disabled / 1 = enabled).
# For testing purposes you can disable the synchronization and then the
# synchronization process is skipped entirely. No '--dry-run' will be
# made and also no synchronization logfile 'rsync.log' will be created.
# ======================================================================
SYNCHRONIZE=1


# ======================================================================
# Specify the backup mountpoint. Here you can specify the mountpoint
# used for the backup. If you specify the mountpoint '/mnt/backup', the
# two partitions ('root' and 'boot') of the SDcard will be mounted the
# following way:
#
#    Partition '/dev/sda2' will be mounted to '/mnt/backup'
#    Partition '/dev/sda1' will be mounted to '/mnt/backup/boot'
#
# ======================================================================
MOUNT_POINT="/mnt/backup"


# ======================================================================
# Specify the path to the synchronization logfile.
# ======================================================================
SYNC_LOG_FILE="/root/backup_pi/rsync.log"


# ======================================================================
# Specify path to the autobackup logfile.
# ======================================================================
AUTOBACKUP_LOG_FILE="/root/backup_pi/autobackup.log"


# ======================================================================
# Specify the script directory. This path will be used to check if the
# script directory exist on the backup SDcard. If not, the backup will
# be canceled. This is for security reasons because we don't want to
# overwrite accidentally an other drive. To disable the directory check
# enter an empty string --> "".
# ======================================================================
SCRIPT_DIRECTORY="/root/backup_pi"


# ======================================================================
# Specify if an OK file will be created after the backup (0 = disabled
# / 1 = enabled). If this setting is enabled, every successful backup
# will write a file to the script directory which has the following
# format:
#
#    OK_2014-09-29_04-42-53
#
# ======================================================================
OK_FILE=0


# ======================================================================
# Specify the minimum size of the boot partition in MB. This setting
# will be used to check that the partition has the right size. If the
# partition size does not match, it is likely that the device is not
# the correct one. In this case the backup will be canceled. To disable
# this check, set the value to 0.
# ======================================================================
MIN_SIZE=47


# ======================================================================
# Specify the maximum size of the boot partition in MB. This setting
# will be used to check that the partition has the right size. If the
# partition size does not match, it is likely that the device is not
# the correct one. In this case the backup will be canceled. To disable
# this check, set the value to 10000.
# ======================================================================
MAX_SIZE=53


# ======================================================================
# Please specify the number of partitions on your backup device. This
# setting will be used to check that the backup device has the right
# number of partitions. If the number of partitions does not match, it
# is likely that the device is not the correct one. In this case the
# backup will be canceled. This setting can't be disabled and normally
# should be set to 2 ('root' partition and 'boot' partition).
# ======================================================================
PARTITION_NUMBER=2


# ======================================================================
# Specify if an e-mail should be sent after each backup (0 = disabled /
# 1 = enabled). The e-mail will be sent with the 'mail' command and you
# have to be sure that a mail transfer agent (MTA) like 'sendmail' or
# 'ssmtp' is properly installed and configured on your system.
# ======================================================================
SEND_EMAIL=1


# ======================================================================
# Specify the e-mail address of the e-mail sender. Here you can enter
# your personal e-mail address or the e-mail address of your host. The
# recipients will see this address as the 'From' address in the mails.
# ======================================================================
EMAIL_SENDER="raspi2@pb-soft.ddns.net"


# ======================================================================
# Specify the e-mail address of the recipient. If you want to specify
# multiple recipients just add them the following way:
#
#    EMAIL_RECIPIENTS="user1@gmx.ch,user2@gmx.ch,user3@gmx.ch"
#
# ======================================================================
EMAIL_RECIPIENTS="biegel@gmx.ch"


# ======================================================================
# Specify if the content of the 'autobackup' logfile should be added to
# the e-mail body (0 = disabled / 1 = enabled). If this setting is en-
# abled, the content of the logfile will be added to the message body
# of the e-mail.
# ======================================================================
EMAIL_AUTO_LOG=1


# ======================================================================
# Specify if the content of the 'rsync' logfile should be added to
# the e-mail body (0 = disabled / 1 = enabled). If this setting is en-
# abled, the content of the logfile will be added to the message body
# of the e-mail.
# ======================================================================
EMAIL_RSYNC_LOG=1


# ======================================================================
# Specify which e-mail application is used to send the backup e-mails.
# If you don't know which application is used on your system you can
# enter the command 'man mail' in a terminal window and you will see
# manpages of your e-mail application. Then choose one from below.
#
#    1 - GNU Mailutils
#    2 - Heirloom mailx
#
# ======================================================================
EMAIL_APPLICATION=1


# ======================================================================
# C O N F I G U R A T I O N   -   N O T   C U S T O M I Z A B L E
# ======================================================================

# Specify the backup script version.
SCRIPT_VERSION=2.0

# Initialize the backup error variable.
BACKUP_ERROR=2

# Initialize the logfile deleted flag.
LOG_DELETED=0


# ======================================================================
# S T A R T   M E S S A G E
# ======================================================================

# Check if the debug mode is enabled and if the terminal variable is set - Begin.
if [[ $DEBUG_MODE == 1 && ${#TERM} -gt 0 && "$TERM" != "dumb" ]]; then

  # Clear the screen.
  clear

  # Write an information message to the screen.
  echo "Raspberry Pi Autobackup - Version $SCRIPT_VERSION"
  echo "Copyright 2014 by PB-Soft"
  echo "The backup was started...."

# Check if the debug mode is enabled and if the terminal variable is set - End.
fi


# ======================================================================
# D E L E T E   O L D   A U T O B A C K U P   L O G F I L E
# ======================================================================

# Check if an old autobackup logfile exist - Begin.
if [ -f "$AUTOBACKUP_LOG_FILE" ]; then

  # Delete the old autobackup logfile.
  rm "$AUTOBACKUP_LOG_FILE"

  # Specify the logfile delete flag.
  LOG_DELETED=1

# Check if an old autobackup logfile exist - End.
fi


# ======================================================================
# I N S E R T   L O G F I L E   S T A R T   I N F O R M A T I O N
# ======================================================================

# Check if the debug mode is enabled - Begin.
if [ $DEBUG_MODE == 1 ]; then

  # Write the logfile start information to the autobackup logfile.
  echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | Raspberry Pi Autobackup - Version $SCRIPT_VERSION - Copyright 2014 by PB-Soft" >> "$AUTOBACKUP_LOG_FILE"

  # Check if the debug mode is enabled.
  if [ $LOG_DELETED == 1 ]; then

    # Write an information message to the autobackup logfile.
    echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The old autobackup logfile '$AUTOBACKUP_LOG_FILE' was deleted." >> "$AUTOBACKUP_LOG_FILE"

  fi

# Check if the debug mode is enabled - End.
fi


# ======================================================================
# G E T   B A C K U P   D E V I C E   N A M E
# ======================================================================

# Check if a backup device was specified via command line parameter - Begin.
if [ "$1" != "" ]; then

  # Check if the debug mode is enabled.
  if [ $DEBUG_MODE == 1 ]; then

    # Write an information message to the autobackup logfile.
    echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The backup device (command line parameter) was set to '$1'." >> "$AUTOBACKUP_LOG_FILE"

  fi

  # Get the name of the backup device from the parameter.
  BACKUP_DEVICE="/dev/$1"

# No command line parameter was set.
else

  # Write an error message to the autobackup logfile.
  echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| ERROR | No backup device (command line parameter) was specified!" >> "$AUTOBACKUP_LOG_FILE"

  # Check if the terminal variable is set - Begin.
  if [[ ${#TERM} -gt 0 && "$TERM" != "dumb" ]]; then

    # Clear the screen.
    clear

    # Write an error message to the screen.
    echo
    echo "================================================================================"
    echo "|        N O   B A C K U P   D E V I C E   W A S   S P E C I F I E D !         |"
    echo "================================================================================"
    echo "|                                                                              |"
    echo "|    There was no backup device specified!                                     |"
    echo "|                                                                              |"
    echo "|    You have to start this script and submit the device name as a parameter.  |"
    echo "|                                                                              |"
    echo "|    Example: Start the script like this: --> autobackup.sh sda                |"
    echo "|                                                                              |"
    echo "================================================================================"
    echo

  # Check if the terminal variable is set - End.
  fi

  # Exit the script.
  exit

# Check if a backup device was specified via command line parameter - End.
fi

# Check if the debug mode is enabled.
if [ $DEBUG_MODE == 1 ]; then

  # Write an information message to the autobackup logfile.
  echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The script variable '\$BACKUP_DEVICE' is set to '$BACKUP_DEVICE'." >> "$AUTOBACKUP_LOG_FILE"

fi


# ======================================================================
# C H E C K   I F   B A C K U P   D E V I C E   W A S   S E T
# ======================================================================

# Check if a backup device was specified - Begin.
if [ "$BACKUP_DEVICE" != "" ]; then

  # Check if the debug mode is enabled.
  if [ $DEBUG_MODE == 1 ]; then

    # Write an information message to the autobackup logfile.
    echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The backup device '$BACKUP_DEVICE' was successfully specified." >> "$AUTOBACKUP_LOG_FILE"

  fi


  # ====================================================================
  # C H E C K   I F   B A C K U P   D E V I C E   E X I S T
  # ====================================================================

  # Check if the selected backup device is available - Begin.
  if [ "/dev/$(lsblk -n -d -o NAME ${BACKUP_DEVICE} 2>&1)" == "${BACKUP_DEVICE}" ]; then

    # Check if the debug mode is enabled.
    if [ $DEBUG_MODE == 1 ]; then

      # Write an information message to the autobackup logfile.
      echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The backup device '$BACKUP_DEVICE' is available." >> "$AUTOBACKUP_LOG_FILE"

    fi


    # ==================================================================
    # C H E C K   N U M B E R   O F   P A R T I T I O N S
    # ==================================================================

    # Check if the selected backup device contains the right number of partitions - Begin.
    if [ "$(/sbin/sfdisk -d $BACKUP_DEVICE 2>&1 | grep "/dev/sd[abcd][12]" | wc -l)" == "$PARTITION_NUMBER" ]; then

      # Check if the debug mode is enabled.
      if [ $DEBUG_MODE == 1 ]; then

        # Write an information message to the autobackup logfile.
        echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The backup device '$BACKUP_DEVICE' contains $PARTITION_NUMBER partitions." >> "$AUTOBACKUP_LOG_FILE"

      fi


      # ================================================================
      # C A L C U L A T E   B O O T   P A R T I T I O N   S I Z E
      # ================================================================

      # Get the line with the size of the first partition from sfdisk.
      RESULT_LINE=$(/sbin/sfdisk -d $BACKUP_DEVICE 2>&1 | grep "${BACKUP_DEVICE}1")

      # Get the partition size in blocks out of the result line.
      PARTITION_SIZE_BLOCKS=`expr "$RESULT_LINE" : '.*size=[ ]*\([0-9]*\)'`

      # Calculate the partition size in MB.
      PARTITION_SIZE_MB=$(($PARTITION_SIZE_BLOCKS*512/1024/1024))


      # ================================================================
      # C H E C K   M I N I M U M   P A R T I T I O N   S I Z E
      # ================================================================

      # Check if the boot partition is bigger or equal the specified minimum size - Begin.
      if [ $PARTITION_SIZE_MB -ge $MIN_SIZE ]; then

        # Check if the debug mode is enabled.
        if [ $DEBUG_MODE == 1 ]; then

          # Write an information message to the autobackup logfile.
          echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The boot partition size is bigger or equal than $MIN_SIZE MB." >> "$AUTOBACKUP_LOG_FILE"

        fi


        # ==============================================================
        # C H E C K   M A X I M U M   P A R T I T I O N   S I Z E
        # ==============================================================

        # Check if the boot partition is smaller or equal the specified maximum size - Begin.
        if [ $PARTITION_SIZE_MB -le $MAX_SIZE ]; then

          # Write an information message to the autobackup logfile.
          echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The boot partition size is smaller or equal than $MAX_SIZE MB." >> "$AUTOBACKUP_LOG_FILE"

          # Check if the debug mode is enabled.
          if [ $DEBUG_MODE == 1 ]; then

            # Write an information message to the autobackup logfile.
            echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The boot partition size is $PARTITION_SIZE_BLOCKS blocks or $PARTITION_SIZE_MB MB." >> "$AUTOBACKUP_LOG_FILE"

          fi


          # ============================================================
          # M O U N T   R O O T   P A R T I T I O N
          # ============================================================

          # Check if the root mountpoint does not exist - Begin.
          if [ ! -d "$MOUNT_POINT" ]; then

            # Create the root mountpoint.
            mkdir -p "$MOUNT_POINT"

            # Check if the debug mode is enabled.
            if [ $DEBUG_MODE == 1 ]; then

              # Write an information message to the autobackup logfile.
              echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The root mountpoint was created." >> "$AUTOBACKUP_LOG_FILE"

            fi

          # Check if the root mountpoint does not exist - End.
          fi

          # Check if the root partition is not already mounted - Begin.
          if [ $(grep "$MOUNT_POINT" /etc/mtab | wc -l) == "0" ]; then

            # Mount the root partition.
            mount "${BACKUP_DEVICE}2" "$MOUNT_POINT"

            # Check if the root partition could not be mounted - Begin.
            if [ $(grep "$MOUNT_POINT" /etc/mtab | wc -l) == "0" ]; then

              # Write an error message to the autobackup logfile.
              echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| ERROR | The root partition could not be mounted!" >> "$AUTOBACKUP_LOG_FILE"

              # Exit the script.
              exit

            # The root partition was successfully mounted.
            else

              # Check if the debug mode is enabled.
              if [ $DEBUG_MODE == 1 ]; then

                # Write an information message to the autobackup logfile.
                echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The root partition was successfully mounted." >> "$AUTOBACKUP_LOG_FILE"

              fi

            # Check if the root partition could not be mounted - End.
            fi

          # The root partition is already mounted.
          else

            # Check if the debug mode is enabled.
            if [ $DEBUG_MODE == 1 ]; then

              # Write an information message to the autobackup logfile.
              echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The root partition is already mounted." >> "$AUTOBACKUP_LOG_FILE"

            fi

          # Check if the root partition is not already mounted - End.
          fi


          # ============================================================
          # M O U N T   B O O T   P A R T I T I O N
          # ============================================================

          # Check if the boot mountpoint does not exist - Begin.
          if [ ! -d "${MOUNT_POINT}/boot" ]; then

            # Create the boot mountpoint.
            mkdir -p "${MOUNT_POINT}/boot"

            # Check if the debug mode is enabled.
            if [ $DEBUG_MODE == 1 ]; then

              # Write an information message to the autobackup logfile.
              echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The boot mountpoint was created." >> "$AUTOBACKUP_LOG_FILE"

            fi

          # Check if the boot mountpoint does not exist - End.
          fi

          # Check if the boot partition is not already mounted - Begin.
          if [ $(grep "$MOUNT_POINT/boot" /etc/mtab | wc -l) == "0" ]; then

            # Mount the boot partition.
            mount "${BACKUP_DEVICE}1" "${MOUNT_POINT}/boot"

            # Check if the boot partition could not be mounted - Begin.
            if [ $(grep "$MOUNT_POINT/boot" /etc/mtab | wc -l) == "0" ]; then

              # Write an error message to the autobackup logfile.
              echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| ERROR | The boot partition could not be mounted!" >> "$AUTOBACKUP_LOG_FILE"

              # Exit the script.
              exit

            # The boot partition was successfully mounted.
            else

              # Check if the debug mode is enabled.
              if [ $DEBUG_MODE == 1 ]; then

                # Write an information message to the autobackup logfile.
                echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The boot partition was successfully mounted." >> "$AUTOBACKUP_LOG_FILE"

              fi

            # Check if the boot partition could not be mounted - End.
            fi

          # The boot partition is already mounted.
          else

            # Check if the debug mode is enabled.
            if [ $DEBUG_MODE == 1 ]; then

              # Write an information message to the autobackup logfile.
              echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The boot partition is already mounted." >> "$AUTOBACKUP_LOG_FILE"

            fi

          # Check if the boot partition is not already mounted - End.
          fi


          # ============================================================
          # S T O P   R U N N I N G   S E R V I C E S
          # ============================================================

          # Check if the debug mode is enabled.
          if [ $DEBUG_MODE == 1 ]; then

            # Write an information message to the autobackup logfile.
            echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The running services will be halted now." >> "$AUTOBACKUP_LOG_FILE"

          fi


          # ============================================================
          # E N T E R   S T O P   C O M M A N D S   H E R E !
          # ============================================================

          #/usr/sbin/service nginx stop > /dev/null
          #/usr/sbin/service mysql stop > /dev/null
          #/usr/sbin/service apcupsd stop > /dev/null


          # ============================================================
          # S E A R C H   F O R   T H E   R S Y N C   T O O L
          # ============================================================

          # Search for the 'rsync' package.
          RSYNC_INSTALLED=$(sudo dpkg-query -l | grep "rsync" | wc -l)

          # Check if the 'rsync' package was found - Begin.
          if [ $RSYNC_INSTALLED == 1 ]; then

            # Check if the debug mode is enabled.
            if [ $DEBUG_MODE == 1 ]; then

              # Write an information message to the autobackup logfile.
              echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The 'rsync' synchronization tool was found." >> "$AUTOBACKUP_LOG_FILE"

            fi


            # ==========================================================
            # D E L E T E   O L D   S Y N C    L O G F I L E
            # ==========================================================

            # Check if an old synchronization logfile exist - Begin.
            if [ -f "$SYNC_LOG_FILE" ]; then

              # Delete the old synchronization logfile.
              rm "$SYNC_LOG_FILE"

              # Check if the debug mode is enabled.
              if [ $DEBUG_MODE == 1 ]; then

                # Write an information message to the autobackup logfile.
                echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The old synchronization logfile '$SYNC_LOG_FILE' was deleted." >> "$AUTOBACKUP_LOG_FILE"

              fi

            # Check if an old synchronization logfile exist - End.
            fi


            # ==========================================================
            # C H E C K   T E S T   M O D E
            # ==========================================================

            # Initialize the dry-run variable.
            DRY_RUN=

            # Check if the test mode is enabled - Begin.
            if [ $TEST_MODE == 1 ]; then

              # Specify the dry-run variable.
              DRY_RUN="--dry-run"

              # Check if the debug mode is enabled.
              if [ $DEBUG_MODE == 1 ]; then

                # Write an information message to the autobackup logfile.
                echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The test mode is enabled (parameter '--dry-run')." >> "$AUTOBACKUP_LOG_FILE"

              fi

            # The test mode is disabled.
            else

              # Check if the debug mode is enabled.
              if [ $DEBUG_MODE == 1 ]; then

                # Write an information message to the autobackup logfile.
                echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The test mode is disabled (parameter '--dry-run')." >> "$AUTOBACKUP_LOG_FILE"

              fi

            # Check if the test mode is enabled - End.
            fi


            # ==========================================================
            # C H E C K   S Y N C H R O N I Z I N G
            # ==========================================================

            # Check if the synchronizing is enabled - Begin.
            if [ $SYNCHRONIZE == 1 ]; then


              # ========================================================
              # C H E C K   S C R I P T   D I R E C T O R Y
              # ========================================================

              # Check if the debug mode is enabled.
              if [ $DEBUG_MODE == 1 ]; then

                # Write an information message to the autobackup logfile.
                echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | Search for the script directory '${MOUNT_POINT}$SCRIPT_DIRECTORY'." >> "$AUTOBACKUP_LOG_FILE"

              fi

              # Check if the script directory on the backup SDcard was found - Begin.
              if [ -d "${MOUNT_POINT}$SCRIPT_DIRECTORY" ]; then

                # Check if the debug mode is enabled.
                if [ $DEBUG_MODE == 1 ]; then

                  # Write an information message to the autobackup logfile.
                  echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The script directory on the backup SDcard was found." >> "$AUTOBACKUP_LOG_FILE"

                fi


                # ======================================================
                # S Y N C H R O N I Z E   F I L E S
                # ======================================================

                # Check if the debug mode is enabled.
                if [ $DEBUG_MODE == 1 ]; then

                  # Write an information message to the autobackup logfile.
                  echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The synchronization was started." >> "$AUTOBACKUP_LOG_FILE"

                fi

                # Synchronize the files - Quiet mode.
                rsync --archive $DRY_RUN --quiet --one-file-system --sparse --stats --delete / /boot "${MOUNT_POINT}/" --log-file="$SYNC_LOG_FILE"

                # Check if the debug mode is enabled.
                if [ $DEBUG_MODE == 1 ]; then

                  # Write an information message to the autobackup logfile.
                  echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The synchronization has terminated." >> "$AUTOBACKUP_LOG_FILE"

                fi

              # The script directory on the backup SDcard does not exist.
              else

                # Write an error message to the autobackup logfile.
                echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| ERROR | The script directory on the backup SDcard does not exist!" >> "$AUTOBACKUP_LOG_FILE"

                # Exit the script.
                exit

              # Check if the script directory on the backup SDcard was found - End.
              fi

            # The synchronizing is disabled.
            else

              # Write an information message to the autobackup logfile.
              echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | Synchronizing files (Demo)..." >> "$AUTOBACKUP_LOG_FILE"

            # Check if the synchronizing is enabled - End.
            fi

          # The 'rsync' package was not found.
          else

            # Write an error message to the autobackup logfile.
            echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| ERROR | The 'rsync' synchronization tool was not found!" >> "$AUTOBACKUP_LOG_FILE"

            # Exit the script.
            exit

          # Check if the 'rsync' package was found - End.
          fi


          # ============================================================
          # C A L C U L A T E   N U M B E R   O F   F I L E S
          # ============================================================

          # Check if the debug mode is enabled - Begin.
          if [ $DEBUG_MODE == 1 ]; then

            # Calculate the number of files on the boot partition.
            BOOT_SOURCE=$(find /boot -mount -type f | wc -l)
            BOOT_DESTINATION=$(find ${MOUNT_POINT}/boot -mount -type f | wc -l)

            # Calculate the number of files on the root partition.
            ROOT_SOURCE=$(find / -mount -type f | wc -l)
            ROOT_DESTINATION=$(find ${MOUNT_POINT}/ -mount -type f | wc -l)

            # Write an information message to the autobackup logfile.
            echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | Number of source files | Boot: $BOOT_SOURCE | Root: $ROOT_SOURCE |" >> "$AUTOBACKUP_LOG_FILE"


            # Write an information message to the autobackup logfile.
            echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | Number of backup files | Boot: $BOOT_DESTINATION | Root: $ROOT_DESTINATION |" >> "$AUTOBACKUP_LOG_FILE"

          # Check if the debug mode is enabled - End.
          fi


          # ============================================================
          # U N M O U N T   B O O T   P A R T I T I O N
          # ============================================================

          # Unmount the boot partition.
          umount "${MOUNT_POINT}/boot"

          # Check if the boot partition was unmounted - Begin.
          if [ $(grep "$MOUNT_POINT/boot" /etc/mtab | wc -l) == "0" ]; then

            # Check if the debug mode is enabled.
            if [ $DEBUG_MODE == 1 ]; then

              # Write an information message to the autobackup logfile.
              echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The boot partition was successfully unmounted." >> "$AUTOBACKUP_LOG_FILE"

            fi

            # Decrease the backup error value.
            BACKUP_ERROR=$(($BACKUP_ERROR - 1))

          # The boot partition could not be unmounted.
          else

            # Write an error message to the autobackup logfile.
            echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| ERROR | The boot partition could not be unmounted!" >> "$AUTOBACKUP_LOG_FILE"

          # Check if the boot partition was unmounted - End.
          fi


          # ============================================================
          # U N M O U N T   R O O T   P A R T I T I O N
          # ============================================================

          # Unmount the root partition.
          umount "$MOUNT_POINT"

          # Check if the root partition was unmounted - Begin.
          if [ $(grep "$MOUNT_POINT" /etc/mtab | wc -l) == "0" ]; then

            # Check if the debug mode is enabled.
            if [ $DEBUG_MODE == 1 ]; then

              # Write an information message to the autobackup logfile.
              echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The root partition was successfully unmounted." >> "$AUTOBACKUP_LOG_FILE"

            fi

            # Decrease the backup error value.
            BACKUP_ERROR=$(($BACKUP_ERROR - 1))

          # The root partition could not be unmounted.
          else

            # Write an error message to the autobackup logfile.
            echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| ERROR | The root partition could not be unmounted!" >> "$AUTOBACKUP_LOG_FILE"

          # Check if the root partition was unmounted - End.
          fi


          # ============================================================
          # S T A R T   S E R V I C E S
          # ============================================================

          # Check if the debug mode is enabled.
          if [ $DEBUG_MODE == 1 ]; then

            # Write an information message to the autobackup logfile.
            echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The services will be started now." >> "$AUTOBACKUP_LOG_FILE"

          fi


          # ============================================================
          # E N T E R   S T A R T   C O M M A N D S   H E R E !
          # ============================================================

          #/usr/sbin/service nginx start > /dev/null
          #/usr/sbin/service mysql start > /dev/null
          #/usr/sbin/service apcupsd start > /dev/null


          # ============================================================
          # F I N A L   I N F O R M A T I O N   M E S S A G E
          # ============================================================

          # Specify the date string for the e-mail message.
          DATE_STRING=`date +"%y%m%d | %d.%m.%Y | %H:%M:%S"`

          # Specify the hostname.
          HOSTNAME=$(hostname)

          # Check if there were no errors - Begin.
          if [ "$BACKUP_ERROR" == "0" ]; then

            # Check if the debug mode is enabled - Begin.
            if [ $DEBUG_MODE == 1 ]; then

              # Write an information message to the autobackup logfile.
              echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| INFO  | The backup was completed successfully!" >> "$AUTOBACKUP_LOG_FILE"

              # Check if the terminal variable is set.
              if [[ ${#TERM} -gt 0 && "$TERM" != "dumb" ]]; then

                # Write an information message to the screen.
                echo "The backup was completed successfully!"

              fi

            # Check if the debug mode is enabled - End.
            fi


            # ==========================================================
            # C R E A T E   E - M A I L   -   S U C C E S S
            # ==========================================================

            # Check if the sending of e-mails is enabled - Begin.
            if [ $SEND_EMAIL == 1 ]; then

              # Specify the e-mail subject.
              EMAIL_SUBJECT="$DATE_STRING | $HOSTNAME | Backup Successful!"

              # Specify the e-mail message.
              EMAIL_MESSAGE="The backup on '$HOSTNAME' was completed successfully!"

            # Check if the sending of e-mails is enabled - End.
            fi


            # ==========================================================
            # C R E A T E   O K   F I L E
            # ==========================================================

            # Check if an OK file should be created.
            if [ $OK_FILE == 1 ]; then

              # Specify the name of the OK file.
              OK_FILE_NAME="OK_`date +"%Y-%m-%d_%H-%M-%S"`"

              # Write an OK file.
              echo "" > "$OK_FILE_NAME"

            fi

          # The backup was not successful.
          else

            # Write an error message to the autobackup logfile.
            echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| ERROR | The backup has failed!" >> "$AUTOBACKUP_LOG_FILE"

            # Check if the terminal variable is set.
            if [[ ${#TERM} -gt 0 && "$TERM" != "dumb" ]]; then

              # Write an error message to the screen.
              echo "The backup has failed!"

            fi


            # ==========================================================
            # C R E A T E   E - M A I L   -   E R R O R
            # ==========================================================

            # Check if the sending of e-mails is enabled - Begin.
            if [ $SEND_EMAIL == 1 ]; then

              # Specify the e-mail subject.
              EMAIL_SUBJECT="$DATE_STRING | $HOSTNAME | Backup Failed!"

              # Specify the e-mail message.
              EMAIL_MESSAGE="The backup on '$HOSTNAME' has failed!"

            # Check if the sending of e-mails is enabled - End.
            fi

          # Check if there were no errors - End.
          fi

          # Check if the sending of e-mails is enabled - Begin.
          if [ $SEND_EMAIL == 1 ]; then


            # ==========================================================
            # A D D   A U T O B A C K U P   L O G   C O N T E N T
            # ==========================================================

            # Check if the output of the autobackup logfile should be sent - Begin.
            if [ $EMAIL_AUTO_LOG == 1 ]; then

              # Check if a new autobackup logfile exist - Begin.
              if [ -f "$AUTOBACKUP_LOG_FILE" ]; then

                # Read the output of the autobackup logfile into a variable.
                AUTO_LOG_OUTPUT=$(< $AUTOBACKUP_LOG_FILE)

                # Add the content of the autobackup logfile to the e-mail message.
                EMAIL_MESSAGE="$EMAIL_MESSAGE\n\n$AUTO_LOG_OUTPUT"

              # Check if a new autobackup logfile exist - End.
              fi

            # Check if the output of the autobackup logfile should be sent - End.
            fi


            # ==========================================================
            # A D D   R S Y N C   L O G   C O N T E N T
            # ==========================================================

            # Check if the output of the synchronization logfile should be sent - Begin.
            if [ $EMAIL_RSYNC_LOG == 1 ]; then

              # Check if a new synchronization logfile exist - Begin.
              if [ -f "$SYNC_LOG_FILE" ]; then

                # Read the output of the synchronization logfile into a variable.
                RSYNC_LOG_OUTPUT=$(< $SYNC_LOG_FILE)

                # Add the content of the synchronization logfile to the e-mail message.
                EMAIL_MESSAGE="$EMAIL_MESSAGE\n\n$RSYNC_LOG_OUTPUT"

              # Check if a new synchronization logfile exist - End.
              fi

            # Check if the output of the synchronization logfile should be sent - End.
            fi


            # ==========================================================
            # S E N D   E - M A I L
            # ==========================================================

            # Check if the e-mail sending application is 'GNU Mailutils'.
            if [ $EMAIL_APPLICATION == 1 ]; then

              # Send e-mail message with 'GNU Mailutils'.
              echo -e "$EMAIL_MESSAGE" | mail.mailutils -s "$EMAIL_SUBJECT" "$EMAIL_RECIPIENTS" -a"From:$EMAIL_SENDER"


            # Check if the e-mail sending application is 'Heirloom mailx'.
            elif [ $EMAIL_APPLICATION == 2 ]; then

              # Send e-mail message with 'Heirloom mailx'.
              echo -e "$EMAIL_MESSAGE" | mailx -r "$EMAIL_SENDER" -s "$EMAIL_SUBJECT" "$EMAIL_RECIPIENTS"

            fi

          # Check if the sending of e-mails is enabled - End.
          fi

        # The size of the boot partition is too big.
        else

          # Write an error message to the autobackup logfile.
          echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| ERROR | The size of the boot partition is bigger than $MAX_SIZE MB!" >> "$AUTOBACKUP_LOG_FILE"

        # Check if the boot partition is smaller or equal the specified maximum size - End.
        fi

      # The size of the boot partition is too small.
      else

        # Write an error message to the autobackup logfile.
        echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| ERROR | The size of the boot partition is smaller than $MIN_SIZE MB!" >> "$AUTOBACKUP_LOG_FILE"

      # Check if the boot partition is bigger or equal the specified minimum size - End.
      fi

    # The selected backup device does not contain the right number of partitions.
    else

      # Write an error message to the autobackup logfile.
      echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| ERROR | The selected backup device does not contain $PARTITION_NUMBER partitions!" >> "$AUTOBACKUP_LOG_FILE"

    # Check if the selected backup device contains the right number of partitions - End.
    fi

  # The selected backup device is not available.
  else

    # Write an error message to the autobackup logfile.
    echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| ERROR | The selected backup device is not available!" >> "$AUTOBACKUP_LOG_FILE"

  # Check if the selected backup device is available - End.
  fi

# No backup device was specified.
else

  # Write an error message to the autobackup logfile.
  echo "|" `date +"%d.%m.%Y | %H:%M:%S"` "| ERROR | No backup device was specified!" >> "$AUTOBACKUP_LOG_FILE"

# Check if a backup device was specified - End.
fi
