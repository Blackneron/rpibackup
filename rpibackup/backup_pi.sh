#!/bin/bash


########################################################################
#                                                                      #
#                                                                      #
#              R A S P B E R R Y   P I   -   B A C K U P               #
#                                                                      #
#                   S D C A R D   T O   S D C A R D                    #
#                    S Y N C H R O N I Z A T I O N                     #
#                                                                      #
#                      G U I D E D   B A C K U P                       #
#                                                                      #
#                      Copyright 2014 by PB-Soft                       #
#                                                                      #
#                           www.pb-soft.com                            #
#                                                                      #
#                                                                      #
########################################################################
#                                                                      #
#      ! ! YOU HAVE TO RUN THIS SCRIPT WITH ROOT PERMISSIONS ! !       #
#                                                                      #
########################################################################
#                                                                      #
#  This bash script will do the following:                             #
#                                                                      #
#  - Search the installed software packages for the necessary ones.    #
#                                                                      #
#  - Install the necessary software packages for using this script.    #
#                                                                      #
#  - Detect the connected block devices of the Raspberry Pi.           #
#                                                                      #
#  - Initialize the selected backup device (create a label, create     #
#    two new partitions - FAT32/EXT4 - and format them).               #
#                                                                      #
#  - Synchronize the data of the main SDcard of the Raspberry Pi to    #
#    the backup SDcard.                                                #
#                                                                      #
########################################################################


# ======================================================================
# C O N F I G U R A T I O N   -   P L E A S E   C U S T O M I Z E
# ======================================================================

# ======================================================================
# Specify if the necessary software should be installed. If enabled,
# the script will check if all the necessary software packages are in-
# stalled and if necessary install them automatically. After the soft-
# ware is installed, this setting can and should be disabled.
# ======================================================================
INSTALL_SOFTWARE=1


# ======================================================================
# Specify if the test mode is enabled (0 = disabled / 1 = enabled). If
# the test mode is enabled, no data will be written to the SDcard. The
# synchronization tool 'rsync' will be started with the command line
# parameter '--dry-run' which will write to the logfile 'rsync.log',
# which files would be copied to the backup device.
# ======================================================================
TEST_MODE=1


# ======================================================================
# Specify if the synchronizing is enabled (0 = disabled / 1 = enabled).
# For testing purposes you can disable the synchronization and then the
# synchronization process is skipped entirely. No '--dry-run' will be
# made and also no synchronization logfile 'rsync.log' will be created.
# ======================================================================
SYNCHRONIZE=1


# ======================================================================
# Specify if the verbose mode is enabled (0 = disabled / 1 = enabled).
# The verbose mode will output more information during the synchroni-
# zation process.
# ======================================================================
VERBOSE_MODE=1


# ======================================================================
# Specify the backup mountpoint. Here you can specify the mountpoint
# used for the backup. If you specify the mountpoint '/mnt/backup', the
# two partitions ('root' and 'boot') of the SDcard will be mounted the
# following way:
#
#    Partition '/dev/sda2' will be mounted to '/mnt/backup'
#    Partition '/dev/sda1' will be mounted to '/mnt/backup/boot'
#
# ======================================================================
MOUNT_POINT="/mnt/backup"


# ======================================================================
# Specify the synchronization logfile.
# ======================================================================
SYNC_LOG_FILE="/root/backup_pi/rsync.log"


# ======================================================================
# Specify if the free/used space should be displayed (0 = disabled /
# 1 = enabled). If enabled, the free and used space on the two SDcards
# will be displayed after the backup operation.
# ======================================================================
DISPLAY_SPACE=1


# ======================================================================
# Specify if the number of files should be displayed (0 = disabled /
# 1 = enabled). If enabled, the number of files on the two SDcards will
# be displayed after the backup operation.
# ======================================================================
FILE_NUMBER=1


# ======================================================================
# Specify if the logfile should be displayed (0 = disabled / 1 = en-
# abled). If enabled, the synchronization logfile 'rsync.log' will be
# displayed after the backup operation.
# ======================================================================
DISPLAY_LOG=1


# ======================================================================
# Specify the major device number (MAJ) of the main SDcard (mmcblk0).
# The major device number of the main SDcard is needed to hide the main
# SDcard from the list of available block devices because we can not
# copy the backup data to the main SDcard (because it is the source).
# ======================================================================
RPI_MAJ=179


# ======================================================================
# Specify the size of the boot partition (FAT32) in MB. If a new SDcard
# is initialized, the first partition (FAT32 boot partition) will have
# the partition size specified here. Normally this size is about 50 MB.
# ======================================================================
BOOT_SIZE=53.5


# ======================================================================
# C O N F I G U R A T I O N   -   N O T   C U S T O M I Z A B L E
# ======================================================================

# Specify the backup script version.
SCRIPT_VERSION=2.0

# Specify the backup device (script parameter 1).
BACKUP_DEVICE=/dev/$1

# Specify the software array which contains the necessary installation
# packages. The tools 'ssmtp' and 'mpack' are only necessary to send
# e-mails with the autobackup script 'autobackup_pi.sh'.
SOFTWARE_ARRAY=("dosfstools" "util-linux" "parted" "rsync" "less" "ssmtp" "mpack")


# ======================================================================
# S T A R T   S C R E E N
# ======================================================================

# Clear the screen.
clear

# Display the 'start' screen.
echo "################################################################################"
echo "#                                                                              #"
echo "#                                                                              #"
echo "#                  R A S P B E R R Y   P I   -   B A C K U P                   #"
echo "#                                                                              #"
echo "#                                                                              #"
echo "#                                 Version: $SCRIPT_VERSION                                 #"
echo "#                                                                              #"
echo "#                       S D C A R D   T O   S D C A R D                        #"
echo "#                        S Y N C H R O N I Z A T I O N                         #"
echo "#                                                                              #"
echo "#                          Copyright 2014 by PB-Soft                           #"
echo "#                                                                              #"
echo "#                               www.pb-soft.com                                #"
echo "#                                                                              #"
echo "#                                                                              #"
echo "################################################################################"
echo
read -p "Press ENTER to continue!"


# ======================================================================
# S O F T W A R E   C H E C K / I N S T A L L A T I O N
# ======================================================================

# Check if the necessary software should be installed - Begin.
if [ $INSTALL_SOFTWARE == 1 ]; then

  # Initialize the installation list.
  INSTALLATION_LIST=


  # ====================================================================
  # S O F T W A R E   C H E C K
  # ====================================================================

  # Clear the screen.
  clear

  # Display the 'software check' screen.
  echo "================================================================================"
  echo "|                         S O F T W A R E   C H E C K                          |"
  echo "================================================================================"
  echo
  echo "Checking the system for the necessary software packages..."
  echo
  echo "================================================================================"
  echo

  # Loop through the software packages - Begin.
  for item in "${SOFTWARE_ARRAY[@]}"
  do

    # Search for the actual package.
    PACKAGE_INSTALLED=$(sudo dpkg-query -l | grep "$item" | wc -l)

    # Check if the actual package was not found.
    if [ $PACKAGE_INSTALLED == 0 ]; then

      # Display an information message.
      echo "---> The '$item' package has to be installed!"

      # Add the actual package to the installation list.
      INSTALLATION_LIST="$INSTALLATION_LIST $item"

    # The actual package was found.
    else

      # Display an information message.
      echo "OK - The '$item' package is already installed..."

    fi

  # Loop through the software packages - End.
  done

  echo
  echo "================================================================================"
  echo
  read -p "Press ENTER to continue!"


  # ====================================================================
  # S O F T W A R E   I N S T A L L A T I O N
  # ====================================================================

  # Check if the necessary software has to be installed - Begin.
  if [ "$INSTALLATION_LIST" != "" ]; then

    # Clear the screen.
    clear

    # Display the 'software installation' screen.
    echo "================================================================================"
    echo "|                  S O F T W A R E   I N S T A L L A T I O N                   |"
    echo "================================================================================"
    echo
    echo "Installing the necessary software packages..."
    echo
    echo "================================================================================"
    echo

    # Update the package lists.
    sudo apt-get update

    # Start the installation of the packages.
    sudo apt-get --force-yes --yes install $INSTALLATION_LIST

    echo
    echo "================================================================================"
    echo
    read -p "Press ENTER to continue!"

  # Check if the necessary software has to be installed - End.
  fi

  # Clear the screen.
  clear

  # Display the 'disable software' screen.
  echo "================================================================================"
  echo "|                 D I S A B L E   S O F T W A R E   C H E C K                  |"
  echo "================================================================================"
  echo
  echo "After this software check/installation, you can disable this part in the"
  echo "configuration section of the script. The next time you run the script, the"
  echo "software check will not run again. To disable this check set the following"
  echo "variable to zero:"
  echo
  echo "INSTALL_SOFTWARE=0"
  echo
  echo "================================================================================"
  echo
  read -p "Press ENTER to continue!"

# Check if the necessary software should be installed - End.
fi


# ======================================================================
# B A C K U P   D E V I C E   S E L E C T I O N
# ======================================================================

# Check if no backup device was specified - Begin.
if [ $1 == ]; then

  # Clear the screen.
  clear

  # Display the 'backup device selection' screen.
  echo "================================================================================"
  echo "|                B A C K U P   D E V I C E   S E L E C T I O N                 |"
  echo "================================================================================"
  echo
  echo "The following block devices were found on your system:"
  echo
  echo "================================================================================"
  echo

  # Display the block devices found with 'lsblk'.
  lsblk -d -o NAME,RM,SIZE -e 1,$RPI_MAJ

  echo
  echo "================================================================================"
  echo
  echo "Please select a backup device!"

  # Specify the text for the select prompt.
  PS3="
  Your choice: "

  # Remove the whitespaces at the begin of the select prompt.
  PS3=${PS3:0:1}${PS3:3:13}

  # Specify the different script options.
  SELECTION=("Device 'sda'" "Device 'sdb'" "Device 'sdc'" "Device 'sdd'" "Display Kernel Log" "Quit")

  # Add some space.
  echo

  # List the different options and wait for an input.
  select OPTION in "${SELECTION[@]}"

  # Loop through the options - Begin.
  do

    # Check if the backup device 'sda' has to be used.
    if [ "$OPTION" == "Device 'sda'" ]; then

      # Specify the backup device.
      BACKUP_DEVICE="/dev/sda"

      # Break the loop.
      break

    # Check if the backup device 'sdb' has to be used.
    elif [ "$OPTION" == "Device 'sdb'" ]; then

      # Specify the backup device.
      BACKUP_DEVICE="/dev/sdb"

      # Break the loop.
      break

    # Check if the backup device 'sdc' has to be used.
    elif [ "$OPTION" == "Device 'sdc'" ]; then

      # Specify the backup device.
      BACKUP_DEVICE="/dev/sdc"

      # Break the loop.
      break

    # Check if the backup device 'sdd' has to be used.
    elif [ "$OPTION" == "Device 'sdd'" ]; then

      # Specify the backup device.
      BACKUP_DEVICE="/dev/sdd"

      # Break the loop.
      break

    # Check if the device information has to be displayed.
    elif [ "$OPTION" == "Display Kernel Log" ]; then

      # Specify the text for the select prompt.
      PS3="Your choice: "

      # Display the kernel log.
      dmesg | less

    # Check if the script should be closed.
    elif [ "$OPTION" == "Quit" ]; then

      # Clear the screen.
      clear

      # Display an information message.
      echo
      echo "The backup script was terminated by the user."
      echo

      # Exit the script.
      exit

    # The entered option is not valid.
    else

      # Specify the text for the select prompt.
      PS3="Your choice: "

      # Display an error message.
      echo "Please select a valid option!"

    fi

  # Loop through the options - End.
  done

# Check if no backup device was specified - End.
fi


# ======================================================================
# B A C K U P   D E V I C E   I N I T I A L I Z A T I O N
# ======================================================================

# Check if the selected backup device is available - Begin.
if [ "/dev/$(lsblk -n -d -o NAME ${BACKUP_DEVICE} 2>&1)" == "${BACKUP_DEVICE}" ]; then

  # Clear the screen.
  clear

  # Display the 'backup device initialization' screen.
  echo "================================================================================"
  echo "|           B A C K U P   D E V I C E   I N I T I A L I Z A T I O N            |"
  echo "================================================================================"
  echo
  echo "The selected backup device '${BACKUP_DEVICE}' contains the following partitions:"
  echo
  echo "================================================================================"
  echo

  # Display the partition information of the selected backup device.
  lsblk -o NAME,SIZE $BACKUP_DEVICE

  echo
  echo "================================================================================"
  echo
  echo "Do you want to initialize the device (create label, partitions and format them)?"
  echo "If you choose yes, ALL DATA on the device '${BACKUP_DEVICE}' WILL BE LOST!"

  # Specify the text for the select prompt.
  PS3="
  Your choice: "

  # Remove the whitespaces at the begin of the select prompt.
  PS3=${PS3:0:1}${PS3:3:13}

  # Specify the different script options.
  SELECTION=("Yes" "No" "Quit")

  # Add some space.
  echo

  # List the different options and wait for an input.
  select OPTION in "${SELECTION[@]}"

  # Loop through the options - Begin.
  do

    # Check if the device should be initialized.
    if [ "$OPTION" == "Yes" ]; then

      # Clear the screen.
      clear

      # Display the 'backup device information' screen.
      echo "================================================================================"
      echo "|              B A C K U P   D E V I C E   I N F O R M A T I O N               |"
      echo "================================================================================"
      echo

      # Display the 'parted' device information.
      parted --script "$BACKUP_DEVICE" print

      echo "================================================================================"
      echo
      echo "DO YOU REALLY WANT TO DELETE ALL DATA?"

      # Specify the different script options.
      SELECTION=("Yes" "No" "Quit")

      # Add some space.
      echo

      # List the different options and wait for an input.
      select OPTION in "${SELECTION[@]}"

      # Loop through the options - Begin.
      do

        # Check if the device should be initialized.
        if [ "$OPTION" == "Yes" ]; then

          # Clear the screen.
          clear

          # Display the 'initializing device...' screen.
          echo "================================================================================"
          echo "|                 I N I T I A L I Z I N G   D E V I C E . . .                  |"
          echo "================================================================================"
          echo
          echo "Labeling device '${BACKUP_DEVICE}'..."
          echo

          # Create a new disk label for the SDcard.
          parted --script "$BACKUP_DEVICE" "mklabel msdos"

          echo "================================================================================"
          echo
          echo "Creating boot partition of ${BOOT_SIZE}MB on device '${BACKUP_DEVICE}'..."
          echo

          # Create the boot partition of the specified size.
          parted --script "$BACKUP_DEVICE" "mkpart primary fat32 1MiB ${BOOT_SIZE}MB"

          echo "================================================================================"
          echo
          echo "Creating root partition (remaining space) on device '${BACKUP_DEVICE}'..."
          echo

          # Create the root partition and use the remaining space of the SDcard.
          parted --script "$BACKUP_DEVICE" "mkpart primary ext4 ${BOOT_SIZE}MB -1s"

          echo "================================================================================"
          echo
          echo "Formatting boot partition (FAT32) on device '${BACKUP_DEVICE}'..."
          echo

          # Format the boot partition (FAT).
          mkfs.vfat "${BACKUP_DEVICE}1"

          echo
          echo "================================================================================"
          echo
          echo "Formatting root partition (EXT4) on device '${BACKUP_DEVICE}'..."
          echo

          # Format the root partition (EXT4).
          mkfs.ext4 -j "${BACKUP_DEVICE}2"

          echo "================================================================================"
          echo
          echo "Display partition information for device '${BACKUP_DEVICE}'..."
          echo

          # Display information about the backup device and the new partitions.
          parted --script "$BACKUP_DEVICE" print

          echo "================================================================================"
          echo
          read -p "Press ENTER to continue!"

          # Break the loop.
          break

        # Check if the backup device should not be initialized.
        elif [ "$OPTION" == "No" ]; then

          # Break the loop.
          break

        # Check if the script should be closed.
        elif [ "$OPTION" == "Quit" ]; then

          # Clear the screen.
          clear

          # Display an information message.
          echo
          echo "The backup script was terminated by the user."
          echo

          # Exit the script.
          exit

        # The entered option is not valid.
        else

          # Specify the text for the select prompt.
          PS3="Your choice: "

          # Display an error message.
          echo "Please select a valid option!"

        fi

      # Loop through the options - End.
      done

      # Break the loop.
      break

    # Check if the backup device should not be initialized.
    elif [ "$OPTION" == "No" ]; then

      # Break the loop.
      break

    # Check if the script should be closed.
    elif [ "$OPTION" == "Quit" ]; then

      # Clear the screen.
      clear

      # Display an information message.
      echo
      echo "The backup script was terminated by the user."
      echo

      # Exit the script.
      exit

    # The entered option is not valid.
    else

      # Specify the text for the select prompt.
      PS3="Your choice: "

      # Display an error message.
      echo "Please select a valid option!"

    fi

  # Loop through the options - End.
  done

# The selected backup device is not available.
else

  # Clear the screen.
  clear

  # Display the 'device not available!' screen.
  echo "================================================================================"
  echo "|                  D E V I C E   N O T   A V A I L A B L E !                   |"
  echo "================================================================================"
  echo
  echo "The selected device '${BACKUP_DEVICE}' is not available!"
  echo
  echo "================================================================================"
  echo

  # Exit the script.
  exit

# Check if the selected backup device is available - End.
fi


# ======================================================================
# B A C K U P   R A S P B E R R Y   P I
# ======================================================================

# Check if the selected backup device contains 2 partitions - Begin.
if [ "$(/sbin/sfdisk -d $BACKUP_DEVICE 2>&1 | grep "/dev/sd[abcd][12]" | wc -l)" == 2 ]; then

  # Clear the screen.
  clear

  # Display the 'backup raspberry pi' screen.
  echo "================================================================================"
  echo "|                    B A C K U P   R A S P B E R R Y   P I                     |"
  echo "================================================================================"
  echo
  echo "Do you really want to backup all the data from the Raspberry Pi computer to the"
  echo "following device?"
  echo
  echo "================================================================================"
  echo
  echo "Selected backup device: '${BACKUP_DEVICE}'"
  echo
  echo "================================================================================"
  echo
  echo "If you choose yes, ALL DATA on the device '${BACKUP_DEVICE}' WILL BE LOST!"

  # Specify the text for the select prompt.
  PS3="
  Your choice: "

  # Remove the whitespaces at the begin of the select prompt.
  PS3=${PS3:0:1}${PS3:3:13}

  # Specify the different script options.
  SELECTION=("Yes" "No" "Quit")

  # Add some space.
  echo

  # List the different options and wait for an input.
  select OPTION in "${SELECTION[@]}"

  # Loop through the options - Begin.
  do

    # Check if the backup should be started.
    if [ "$OPTION" == "Yes" ]; then


      # ================================================================
      # S T E P   1   -   M O U N T I N G   I N F O R M A T I O N
      # ================================================================

      # Clear the screen.
      clear

      # Display the 'mounting information' screen.
      echo "================================================================================"
      echo "|                  M O U N T I N G   I N F O R M A T I O N                     |"
      echo "================================================================================"
      echo
      echo "The selected backup device is: '$BACKUP_DEVICE'"
      echo
      echo "The partition '${BACKUP_DEVICE}2' will be mounted to '$MOUNT_POINT'"
      echo "The partition '${BACKUP_DEVICE}1' will be mounted to '${MOUNT_POINT}/boot'"

      # Check if the test mode is enabled.
      if [ $TEST_MODE == 1 ]; then

        # Display the test mode.
        echo
        echo "--> The TEST MODE is active (no data will be written)!"

      fi

      echo
      echo "Please check if the settings are correct before you continue!"
      echo
      echo "================================================================================"
      echo
      read -p "Press ENTER to continue or CTRL-C to abort!"


      # ================================================================
      # S T E P   2   -   M O U N T   R O O T   P A R T I T I O N
      # ================================================================

      # Clear the screen.
      clear

      # Display the 'mounting partitions...' screen.
      echo "================================================================================"
      echo "|                M O U N T I N G   P A R T I T I O N S . . .                   |"
      echo "================================================================================"
      echo
      echo "Mounting the root partition..."

      # Check if the root mountpoint does not exist.
      if [ ! -d "$MOUNT_POINT" ]; then

        # Create the root mountpoint.
        mkdir -p "$MOUNT_POINT"

      fi

      # Check if the root partition is already mounted.
      if grep "$MOUNT_POINT" /etc/mtab > /dev/null 2>&1; then

        # The root partition is already mounted.
        echo "--> Partition '${BACKUP_DEVICE}2' is already mounted to '$MOUNT_POINT'!"

      # The root partition has to be mounted - Begin.
      else

        # Mount the root partition.
        mount "${BACKUP_DEVICE}2" "$MOUNT_POINT"

        # Check if the root partition could be mounted.
        if grep "$MOUNT_POINT" /etc/mtab > /dev/null 2>&1; then

          # The root partition is mounted.
          echo "--> Partition '${BACKUP_DEVICE}2' was mounted to '$MOUNT_POINT'!"

        # There was an error mounting the root partition.
        else

          # Display the 'mounting error (root)!' screen.
          echo "================================================================================"
          echo "|                 M O U N T I N G   E R R O R   ( R O O T ) !                  |"
          echo "================================================================================"
          echo
          echo "--> The root partition could not be mounted!"
          echo
          echo "================================================================================"
          echo

          #Exit the script.
          exit

        fi

      # The root partition has to be mounted - End.
      fi


      # ================================================================
      # S T E P   3   -   M O U N T   B O O T   P A R T I T I O N
      # ================================================================

      # Display an information message about mounting the boot partition.
      echo
      echo "Mounting the boot partition..."

      # Check if the boot mountpoint does not exist.
      if [ ! -d "${MOUNT_POINT}/boot" ]; then

        # Create the boot mountpoint.
        mkdir -p "${MOUNT_POINT}/boot"

      fi

      # Check if the boot partition is already mounted.
      if grep "${MOUNT_POINT}/boot" /etc/mtab > /dev/null 2>&1; then

        # The boot partition is already mounted.
        echo "--> Partition '${BACKUP_DEVICE}1' is already mounted to '${MOUNT_POINT}/boot'!"

      # The boot partition has to be mounted - Begin.
      else

        # Mount the boot partition.
        mount "${BACKUP_DEVICE}1" "${MOUNT_POINT}/boot"

        # Check if the boot partition could be mounted.
        if grep "${MOUNT_POINT}/boot" /etc/mtab > /dev/null 2>&1; then

          # The boot partition is mounted.
          echo "--> Partition '${BACKUP_DEVICE}1' was mounted to '${MOUNT_POINT}/boot'!"

        # There was an error mounting the boot partition.
        else

          # Display the 'mounting error (boot)!' screen.
          echo "================================================================================"
          echo "|                 M O U N T I N G   E R R O R   ( B O O T ) !                  |"
          echo "================================================================================"
          echo
          echo "--> The boot partition could not be mounted!"
          echo
          echo "================================================================================"
          echo

          #Exit the script.
          exit

        fi

      # The boot partition has to be mounted - End.
      fi

      echo
      echo "The mounting process has completed!"
      echo
      echo "================================================================================"
      echo
      read -p "Press ENTER to continue!"


      # ================================================================
      # S T E P   4   -   S T O P   R U N N I N G   S E R V I C E S
      # ================================================================

      # Clear the screen.
      clear

      # Display the 'stop running services...' screen.
      echo "================================================================================"
      echo "|              S T O P   R U N N I N G   S E R V I C E S . . .                 |"
      echo "================================================================================"
      echo


      # ================================================================
      # E N T E R   S T O P   C O M M A N D S   H E R E !
      # ================================================================
      echo "---> No stop services are specified..."
      echo
      echo "     Please edit the corresponding script section to add services!"
      #service nginx stop
      #service mysql stop
      #service apcupsd stop

      echo
      echo "The necessary services were stopped!"
      echo
      echo "================================================================================"
      echo
      read -p "Press ENTER to continue!"


      # ================================================================
      # S T E P   5   -   S Y N C H R O N I Z E   F I L E S
      # ================================================================

      # Clear the screen.
      clear

      # Display the 'synchronize files...' screen.
      echo "================================================================================"
      echo "|                  S Y N C H R O N I Z E   F I L E S . . .                     |"
      echo "================================================================================"

      # Check if an old synchronization logfile exist.
      if [ -f "$SYNC_LOG_FILE" ]; then

        # Delete the old synchronization logfile.
        rm "$SYNC_LOG_FILE"

      fi

      # Initialize the dry-run variable.
      DRY_RUN=

      # Check if the test mode is enabled.
      if [ $TEST_MODE == 1 ]; then

        # Specify the dry-run variable.
        DRY_RUN="--dry-run"

        # Display an information message about the test mode.
        echo
        echo "The backup process was started, please wait... (TEST MODE)"

      # The test mode is disabled.
      else

        # Display an information message about the test mode.
        echo
        echo "The backup process was started, please wait... (REAL MODE)"

      fi

      echo
      echo "================================================================================"


      # ================================================================
      # C H E C K   S Y N C H R O N I Z I N G
      # ================================================================

      # Check if the synchronizing is enabled - Begin.
      if [ $SYNCHRONIZE == 1 ]; then

        # Check if the verbose mode is enabled.
        if [ $VERBOSE_MODE == 1 ]; then

          # Synchronize the files - Verbose mode.
          echo
          rsync --archive $DRY_RUN --verbose --one-file-system --sparse --stats --delete / /boot "${MOUNT_POINT}/" --log-file="$SYNC_LOG_FILE"

        # The verbose mode is disabled.
        else

          # Synchronize the files - Quiet mode.
          rsync --archive $DRY_RUN --quiet --one-file-system --sparse --stats --delete / /boot "${MOUNT_POINT}/" --log-file="$SYNC_LOG_FILE"

        fi

      # The synchronizing is disabled.
      else

        # Displaying a warning message.
        echo
        echo "Synchronizing is disabled!"
        echo
        echo "You can enable it in the configuration section of the script!"
        echo
        echo "SYNCHRONIZE=1"

      # Check if the synchronizing is enabled - End.
      fi

      echo
      echo "================================================================================"
      echo
      read -p "Press ENTER to continue!"


      # ================================================================
      # S T E P   6   -   D I S P L A Y   F R E E / U S E D   S P A C E
      # ================================================================

      # Check if the free/used space should be displayed - Begin.
      if [ $DISPLAY_SPACE == 1 ]; then

        # Clear the screen.
        clear

        # Display the 'free/used space' screen.
        echo "================================================================================"
        echo "|                       F R E E / U S E D   S P A C E                          |"
        echo "================================================================================"
        echo
        echo "Displaying the free/used space..."
        echo
        echo "================================================================================"
        echo

        # Display the free/used space of the boot partition.
        df -T --type=vfat

        echo
        echo "================================================================================"
        echo

        # Display the free/used space of the root partition.
        df -T --type=ext4

        echo
        echo "================================================================================"
        echo
        read -p "Press ENTER to continue!"

      # Check if the free/used space should be displayed - End.
      fi


      # ================================================================
      # S T E P   7   -   D I S P L A Y   N U M B E R   O F   F I L E S
      # ================================================================

      # Check if the number of files should be displayed - Begin.
      if [ $FILE_NUMBER == 1 ]; then

        # Clear the screen.
        clear

        # Display the 'number of files' screen.
        echo "================================================================================"
        echo "|                       N U M B E R   O F   F I L E S                          |"
        echo "================================================================================"
        echo
        echo "Displaying the number of files..."
        echo
        echo "================================================================================"
        echo

        # Calculate the number of files on the boot partition.
        BOOT_SOURCE=$(find /boot -mount -type f | wc -l)
        BOOT_DESTINATION=$(find ${MOUNT_POINT}/boot -mount -type f | wc -l)

        # Display the number of files on the boot partition.
        echo "Boot partition (source): $BOOT_SOURCE files (/boot)"
        echo "Boot partition (backup): $BOOT_DESTINATION files (${MOUNT_POINT}/boot)"
        echo
        echo "================================================================================"
        echo

        # Calculate the number of files on the root partition.
        ROOT_SOURCE=$(find / -mount -type f | wc -l)
        ROOT_DESTINATION=$(find ${MOUNT_POINT}/ -mount -type f | wc -l)

        # Display the number of files on the root partition.
        echo "Root partition (source): $ROOT_SOURCE files (/)"
        echo "Root partition (backup): $ROOT_DESTINATION files (${MOUNT_POINT}/)"

        echo
        echo "================================================================================"
        echo
        read -p "Press ENTER to continue!"

      # Check if the number of files should be displayed - End.
      fi


      # ================================================================
      # S T E P   8   -   U N M O U N T   B O O T   P A R T I T I O N
      # ================================================================

      # Clear the screen.
      clear

      # Display the 'unmounting partitions...' screen.
      echo "================================================================================"
      echo "|              U N M O U N T I N G   P A R T I T I O N S . . .                 |"
      echo "================================================================================"
      echo
      echo "Unmounting the boot partition..."

      # Unmount the boot partition.
      umount "${MOUNT_POINT}/boot"

      # Check if the boot partition could not be unmounted.
      if grep "${MOUNT_POINT}/boot" /etc/mtab > /dev/null 2>&1; then

        # The boot partition could not be unmounted.
        echo "--> The boot partition could not be unmounted!"

      # Unmounting the boot partition was successful.
      else

        # The boot partition was successfully unmounted.
        echo "--> Partition '${BACKUP_DEVICE}1' was successfully unmounted!"

      fi


      # ================================================================
      # S T E P   9   -   U N M O U N T   B O O T   P A R T I T I O N
      # ================================================================

      # Display an information message about unmounting the root partition.
      echo
      echo "Unmounting the root partition..."

      # Unmount the root partition.
      umount "$MOUNT_POINT"

      # Check if the root partition could not be unmounted.
      if grep "$MOUNT_POINT" /etc/mtab > /dev/null 2>&1; then

        # The root partition could not be unmounted.
        echo "--> The root partition could not be unmounted!"

      # Unmounting the root partition was successful.
      else

        # The root partition was successfully unmounted.
        echo "--> Partition '${BACKUP_DEVICE}2' was successfully unmounted!"

      fi

      echo
      echo "The unmounting process has completed!"
      echo
      echo "================================================================================"
      echo
      read -p "Press ENTER to continue!"


      # ================================================================
      # S T E P   10   -   D I S P L A Y   S Y N C   L O G F I L E
      # ================================================================

      # Check if the logfile should be displayed - Begin.
      if [ $DISPLAY_LOG == 1 ]; then

        # Clear the screen.
        clear

        # Display the 'display synchronization logfile' screen.
        echo "================================================================================"
        echo "|       D I S P L A Y   S Y N C H R O N I Z A T I O N   L O G F I L E          |"
        echo "================================================================================"
        echo
        echo "The backup logfile will now be displayed."
        echo
        echo "After you have checked the logfile, press 'q' to exit."
        echo
        echo "================================================================================"
        echo
        read -p "Press ENTER to continue or CTRL-C to abort!"

        # Clear the screen.
        clear

        # Display the 'synchronization logfile' screen.
        echo "================================================================================"
        echo "|               S Y N C H R O N I Z A T I O N   L O G F I L E                  |"
        echo "================================================================================"
        echo

        # Display the synchronization logfile.
        cat "$SYNC_LOG_FILE" | less

      # Check if the logfile should be displayed - End.
      fi


      # ================================================================
      # S T E P   11   -   S T A R T   S E R V I C E S
      # ================================================================

      # Clear the screen.
      clear

      # Display the 'start services...' screen.
      echo "================================================================================"
      echo "|                     S T A R T   S E R V I C E S . . .                        |"
      echo "================================================================================"
      echo


      # ================================================================
      # E N T E R   S T A R T   C O M M A N D S   H E R E !
      # ================================================================
      echo "---> No start services are specified..."
      echo
      echo "     Please edit the corresponding script section to add services!"
      #service nginx start
      #service mysql start
      #service apcupsd start

      echo
      echo "The necessary services were started!"


      # ================================================================
      # S T E P   12   -   F I N A L   M E S S A G E
      # ================================================================

      # Display a final information message.
      echo
      echo "The Raspberry Pi backup was completed successfully!"
      echo
      echo "================================================================================"
      echo
      read -p "Press ENTER to finish!"

      # Break the loop.
      break

    # Check if the backup should not be started.
    elif [ "$OPTION" == "No" ]; then

      # Break the loop.
      break

    # Check if the script should be closed.
    elif [ "$OPTION" == "Quit" ]; then

      # Clear the screen.
      clear

      # Display an information message.
      echo
      echo "The backup script was terminated by the user."
      echo

      # Exit the script.
      exit

    # The entered option is not valid.
    else

      # Specify the text for the select prompt.
      PS3="Your choice: "

      # Display an error message.
      echo "Please select a valid option!"

    fi

  # Loop through the options - End.
  done

# The device does not contain 2 partitions.
else

  # Clear the screen.
  clear

  # Display the 'device not available!' screen.
  echo "================================================================================"
  echo "|                       P A R T I T I O N   E R R O R !                        |"
  echo "================================================================================"
  echo
  echo "The selected device does not contain 2 partitions!"
  echo
  echo "================================================================================"
  echo

  # Exit the script.
  exit

# Check if the selected backup device contains 2 partitions - End.
fi

# Clear the screen.
clear

# Display a final information message.
echo
echo "The backup script has terminated normally."
echo
